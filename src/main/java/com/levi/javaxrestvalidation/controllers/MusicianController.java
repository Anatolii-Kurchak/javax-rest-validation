package com.levi.javaxrestvalidation.controllers;

import com.levi.javaxrestvalidation.api.MusiciansApi;
import com.levi.javaxrestvalidation.model.Musician;
import com.levi.javaxrestvalidation.validators.musician.MusicianGroupSequence;
import com.levi.javaxrestvalidation.validators.musician.NameAndSurnameStartsWithSameLetter;
import com.levi.javaxrestvalidation.validators.musician.ValidAge;
import com.levi.javaxrestvalidation.validators.musician.ValidSurname;
import com.levi.javaxrestvalidation.validators.song.ValidName;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class MusicianController implements MusiciansApi {

    @Override
    @Validated(MusicianGroupSequence.class)
    @NameAndSurnameStartsWithSameLetter(groups = NameAndSurnameStartsWithSameLetter.Group.class)
    public ResponseEntity<List<Musician>> getMusicians(
            @ValidName(groups = ValidName.Group.class)
            String name,
            @ValidSurname(groups = ValidSurname.Group.class)
            String surname,
            @ValidAge(groups = ValidAge.Group.class, min = 10, max = 35)
            Integer age) {

        return ResponseEntity.ok(Collections.emptyList());
    }
}
