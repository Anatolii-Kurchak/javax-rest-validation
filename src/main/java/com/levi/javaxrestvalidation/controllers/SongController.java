package com.levi.javaxrestvalidation.controllers;

import com.levi.javaxrestvalidation.api.SongsApi;
import com.levi.javaxrestvalidation.model.Song;
import com.levi.javaxrestvalidation.service.SongService;
import com.levi.javaxrestvalidation.validators.song.PositiveNumber;
import com.levi.javaxrestvalidation.validators.song.ValidName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SongController implements SongsApi {

    private final SongService songService;

    @Autowired
    public SongController(SongService songService) {
        this.songService = songService;
    }

    @Override
    public ResponseEntity<List<Song>> getSongs(
            @ValidName String title,
            @PositiveNumber Integer duration) {

        return ResponseEntity.ok(songService.getSongs(title, duration));
    }
}