package com.levi.javaxrestvalidation.controllers;

import com.levi.javaxrestvalidation.model.MusicStudio;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/studios")
//@Validated
public class MusicStudioController {

    @PostMapping
//  @Validated(StudioGroupSequence.class)/* for sequential validation*/
    public ResponseEntity<MusicStudio> createMusicStudio(//on controller layer it'll throw MethodArgumentNotValidException
            @RequestBody
            @Valid MusicStudio studio) { // for max deep validation
        return ResponseEntity.ok(studio);
    }
}
