package com.levi.javaxrestvalidation.controllers;

import com.levi.javaxrestvalidation.api.GroupsApi;
import com.levi.javaxrestvalidation.model.MusicGroup;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class MusicGroupController implements GroupsApi {

    @Override
    public ResponseEntity<MusicGroup> createGroup(@Valid MusicGroup musicGroup) {
        return ResponseEntity.ok(musicGroup);
    }
}
