package com.levi.javaxrestvalidation.service;

import com.google.common.collect.Lists;
import com.levi.javaxrestvalidation.model.Song;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class SongService {

    private final List<Song> songs = Lists.newArrayList(
            new Song().title("third").duration(3),
            new Song().title("fourth").duration(4),
            new Song().title("fifth").duration(5),
            new Song().title("sixth").duration(6)
    );

    public List<Song> getSongs(String name, Integer duration) {
        Predicate<Song> songFilter = getSongFilter(name, duration);

        return songs.stream()
                .filter(songFilter)
                .collect(Collectors.toList());
    }

    private Predicate<Song> getSongFilter(String name, Integer duration) {
        Predicate<Song> songFilter = s -> true;

        if (StringUtils.isNotBlank(name)) {
            songFilter = songFilter.and(s -> s.getTitle().equalsIgnoreCase(name));
        }

        if (duration != null) {
            songFilter = songFilter.and(s -> s.getDuration().equals(duration));
        }

        return songFilter;
    }
}
