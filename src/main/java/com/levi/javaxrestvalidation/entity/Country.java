package com.levi.javaxrestvalidation.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "COUNTRY")
public class Country {

    @Id
    @Column(name = "ISO3")
    private String iso3;

    @Column(name = "NAME")
    private String name;
}
