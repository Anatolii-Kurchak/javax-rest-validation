package com.levi.javaxrestvalidation.validators.album;

import com.levi.javaxrestvalidation.util.AlbumType;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidAlbumTypesListValidator.class)
public @interface ValidAlbumTypesList {

    String message() default "{com.levi.javax.rest.validation.album.type}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    AlbumType[] allowedTypes();

    interface Group{}
}
