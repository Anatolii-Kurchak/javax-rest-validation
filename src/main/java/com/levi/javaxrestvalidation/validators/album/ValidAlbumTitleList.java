package com.levi.javaxrestvalidation.validators.album;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidAlbumTitleListValidator.class)
public @interface ValidAlbumTitleList {

    String message() default "{com.levi.javax.rest.validation.album.title}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    interface Group{}
}
