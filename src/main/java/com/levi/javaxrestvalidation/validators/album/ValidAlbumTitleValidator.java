package com.levi.javaxrestvalidation.validators.album;

import com.levi.javaxrestvalidation.util.SkipValidationRule;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidAlbumTitleValidator implements ConstraintValidator<ValidAlbumTitle, String> {

   private SkipValidationRule skipValidationRule;

   public void initialize(ValidAlbumTitle annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(String title, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(title)) {
         return true;
      }

      return StringUtils.isNotBlank(title) && title.toLowerCase().contains("success");
   }
}
