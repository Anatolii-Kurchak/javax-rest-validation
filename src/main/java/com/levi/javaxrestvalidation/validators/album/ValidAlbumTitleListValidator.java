package com.levi.javaxrestvalidation.validators.album;

import com.levi.javaxrestvalidation.model.Album;
import com.levi.javaxrestvalidation.util.HibernateConstraintService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ValidAlbumTitleListValidator implements ConstraintValidator<ValidAlbumTitleList, List<Album>> {

   @Autowired
   private HibernateConstraintService hibernateConstraintService;

   private String message;

   @Override
   public void initialize(ValidAlbumTitleList annotation) {
      this.message = annotation.message();
   }

   public boolean isValid(List<Album> albums, ConstraintValidatorContext context) {
      List<String> invalidValues = albums.stream()
              .map(Album::getTitle)
              .filter(Objects::nonNull)
              .filter(title -> !title.toLowerCase().contains("success"))
              .collect(Collectors.toList());

      boolean valid = CollectionUtils.isEmpty(invalidValues);

      if (!valid) {
         hibernateConstraintService.addConstraintViolations(context, message, invalidValues);
      }

      return valid;
   }
}
