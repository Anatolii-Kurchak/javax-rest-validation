package com.levi.javaxrestvalidation.validators.album;

import com.levi.javaxrestvalidation.model.Album;
import com.levi.javaxrestvalidation.util.HibernateConstraintService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValidAlbumTypesListValidator implements ConstraintValidator<ValidAlbumTypesList, List<Album>> {

   @Autowired
   private HibernateConstraintService hibernateConstraintService;

   private List<String> allowedAlbumTypes;
   private String messageTemplate;

   @Override
   public void initialize(ValidAlbumTypesList annotation) {
      this.messageTemplate = annotation.message();
      this.allowedAlbumTypes = Stream.of(annotation.allowedTypes())
      .map(Enum::name)
      .map(String::toLowerCase)
      .collect(Collectors.toList());
   }

   public boolean isValid(List<Album> albums, ConstraintValidatorContext context) {
      List<String> invalidTypes = albums.stream()
              .map(Album::getAlbumType)
              .filter(Objects::nonNull)
              .filter(type -> !allowedAlbumTypes.contains(type.toLowerCase()))
              .collect(Collectors.toList());

      boolean valid = CollectionUtils.isEmpty(invalidTypes);

      if (!valid) {
         hibernateConstraintService.addConstraintViolations(context, messageTemplate, invalidTypes);
      }

      return valid;
   }
}
