package com.levi.javaxrestvalidation.validators.album;

import com.levi.javaxrestvalidation.util.AlbumType;
import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.stream.Stream;

public class ValidAlbumTypeValidator implements ConstraintValidator<ValidAlbumType, String> {

   private AlbumType[] allowedAlbumTypes;

   private SkipValidationRule skipValidationRule;

   public void initialize(ValidAlbumType annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
      this.allowedAlbumTypes = annotation.allowedTypes();
   }

   public boolean isValid(String albumType, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(albumType)) {
         return true;
      }

      return Stream.of(allowedAlbumTypes)
              .map(Enum::name)
              .anyMatch(type -> type.equalsIgnoreCase(albumType));
   }
}
