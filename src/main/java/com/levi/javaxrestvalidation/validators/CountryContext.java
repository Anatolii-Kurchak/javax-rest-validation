package com.levi.javaxrestvalidation.validators;

import com.levi.javaxrestvalidation.entity.Country;
import com.levi.javaxrestvalidation.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CountryContext {

    private final CountryRepository countryRepository;

    private Map<String, Country> countriesByIso3 = new HashMap<>();

    @Autowired
    public CountryContext(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public Country findByIso3(String iso3) {
        Country country = countriesByIso3.get(iso3);

        if (country == null) {
            country = countryRepository.findByIso3(iso3);
            countriesByIso3.put(iso3, country);
        }

        return country;
    }
}
