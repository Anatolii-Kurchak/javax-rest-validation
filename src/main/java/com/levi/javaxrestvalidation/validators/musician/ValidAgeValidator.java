package com.levi.javaxrestvalidation.validators.musician;

import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidAgeValidator implements ConstraintValidator<ValidAge, Integer> {

   private int min;
   private int max;
   private SkipValidationRule skipValidationRule;

   public void initialize(ValidAge annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
      this.min = annotation.min();
      this.max = annotation.max();
   }

   public boolean isValid(Integer age, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(age)) {
         return true;
      }

      return min <= age && age <= max;
   }
}
