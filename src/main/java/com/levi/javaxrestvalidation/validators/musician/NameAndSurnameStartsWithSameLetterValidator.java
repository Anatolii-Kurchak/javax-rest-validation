package com.levi.javaxrestvalidation.validators.musician;

import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

@SupportedValidationTarget(value = ValidationTarget.PARAMETERS)
public class NameAndSurnameStartsWithSameLetterValidator implements ConstraintValidator<NameAndSurnameStartsWithSameLetter, Object[]> {

   private static final int NAME = 0;
   private static final int SURNAME = 1;
   private static final int FIRST_LETTER = 0;
   private SkipValidationRule skipValidationRule;

   @Override
   public void initialize(NameAndSurnameStartsWithSameLetter annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(Object[] parameters, ConstraintValidatorContext context) {
      String name = (String)parameters[NAME];
      String surname = (String)parameters[SURNAME];

      if (shouldSkipValidation(name, surname)) {
         return true;
      }

      return name.charAt(FIRST_LETTER) == surname.charAt(FIRST_LETTER);
   }

   private boolean shouldSkipValidation(String name, String surname) {
      return skipValidationRule.shouldSkipValidation(name) || skipValidationRule.shouldSkipValidation(surname);
   }
}
