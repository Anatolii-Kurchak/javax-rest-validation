package com.levi.javaxrestvalidation.validators.musician;

import com.levi.javaxrestvalidation.validators.song.ValidName;

import javax.validation.GroupSequence;

@GroupSequence({
        ValidName.Group.class,
        ValidSurname.Group.class,
        NameAndSurnameStartsWithSameLetter.Group.class,
        ValidAge.Group.class
})
public interface MusicianGroupSequence {
}
