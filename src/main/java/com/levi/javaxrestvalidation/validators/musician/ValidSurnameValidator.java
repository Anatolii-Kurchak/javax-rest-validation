package com.levi.javaxrestvalidation.validators.musician;

import com.levi.javaxrestvalidation.util.SkipValidationRule;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidSurnameValidator implements ConstraintValidator<ValidSurname, String> {

   private SkipValidationRule skipValidationRule;

   @Override
   public void initialize(ValidSurname annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(String name, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(name)) {
         return true;
      }

      return StringUtils.isNotBlank(name) && StringUtils.isAlpha(name);
   }
}
