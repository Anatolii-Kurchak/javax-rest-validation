package com.levi.javaxrestvalidation.validators.song;

import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PositiveNumberValidator.class)
public @interface PositiveNumber {

    String message() default "{com.levi.javax.rest.validation.number}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    SkipValidationRule skipValidationRule() default SkipValidationRule.OBJECT_IS_NULL;

    interface Group{}
}
