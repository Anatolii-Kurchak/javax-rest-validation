package com.levi.javaxrestvalidation.validators.song;

import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PositiveNumberValidator implements ConstraintValidator<PositiveNumber, Integer> {

   private SkipValidationRule skipValidationRule;

   @Override
   public void initialize(PositiveNumber annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(Integer number, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(number)) {
         return true;
      }

      return number != null && number > 0;
   }
}
