package com.levi.javaxrestvalidation.validators.contributor;

import com.levi.javaxrestvalidation.model.MusicGroup;
import com.levi.javaxrestvalidation.util.AlbumType;
import com.levi.javaxrestvalidation.validators.album.ValidAlbumTitle;
import com.levi.javaxrestvalidation.validators.album.ValidAlbumTitleList;
import com.levi.javaxrestvalidation.validators.album.ValidAlbumType;
import com.levi.javaxrestvalidation.validators.album.ValidAlbumTypesList;
import com.levi.javaxrestvalidation.validators.song.PositiveNumber;
import com.levi.javaxrestvalidation.validators.song.ValidName;
import org.hibernate.validator.cfg.ConstraintDef;
import org.hibernate.validator.cfg.GenericConstraintDef;
import org.hibernate.validator.spi.cfg.ConstraintMappingContributor;

public class MusicGroupConstraintMappingContributor implements ConstraintMappingContributor {
    @Override
    public void createConstraintMappings(ConstraintMappingBuilder builder) {
        //        Deep breath validation
        /*builder.addConstraintMapping()
                .type(MusicGroup.class)
                .ignoreAllAnnotations()
                .field("name").constraint(new GenericConstraintDef<>(ValidName.class))
                .field("participantsAmount").constraint(new GenericConstraintDef<>(PositiveNumber.class))
                .field("albums").valid();

        builder.addConstraintMapping()
                .type(Album.class)
                .ignoreAllAnnotations()
                .field("title").constraint(createAlbumTitleConstraint(false))
                .field("albumType").constraint(createAlbumTypeConstraint(false, AlbumType.DEMO_ALBUM, AlbumType.REMIX_ALBUM));*/


//        Deep breath validation with group sequence on Album level
        /*builder.addConstraintMapping()
                .type(MusicGroup.class)
                .ignoreAllAnnotations()
                .field("name").constraint(new GenericConstraintDef<>(ValidName.class))
                .field("participantsAmount").constraint(new GenericConstraintDef<>(PositiveNumber.class))
                .field("albums").valid();

        builder.addConstraintMapping()
                .type(Album.class)
                .ignoreAllAnnotations()
                .defaultGroupSequence(
                        ValidAlbumTitle.Group.class,
                        ValidAlbumType.Group.class,
                        Album.class
                )
                .field("title").constraint(createAlbumTitleConstraint(true))
                .field("albumType").constraint(createAlbumTypeConstraint(true, AlbumType.DEMO_ALBUM, AlbumType.REMIX_ALBUM));
*/

//        Fail fast validation
        builder.addConstraintMapping()
                .type(MusicGroup.class)
                .ignoreAllAnnotations()
                .defaultGroupSequence(
                        ValidName.Group.class,
                        PositiveNumber.Group.class,
                        ValidAlbumTitleList.Group.class,
                        ValidAlbumTypesList.Group.class,
                        MusicGroup.class
                )
                .field("name").constraint(createValidNameConstraint(true))
                .field("participantsAmount").constraint(createPositiveNumberConstraint(true))
                .field("albums")
                    .constraint(new GenericConstraintDef<>(ValidAlbumTitleList.class)
                            .groups(ValidAlbumTitleList.Group.class))
                    .constraint(new GenericConstraintDef<>(ValidAlbumTypesList.class)
                            .groups(ValidAlbumTypesList.Group.class)
                            .param("allowedTypes", new AlbumType[]{AlbumType.DEMO_ALBUM, AlbumType.SOLO}));

    }

    private ConstraintDef createValidNameConstraint(boolean includeGroup) {
        GenericConstraintDef<ValidName> constraint = new GenericConstraintDef<>(ValidName.class);

        if (includeGroup) {
            constraint.groups(ValidName.Group.class);
        }

        return constraint;
    }

    private ConstraintDef createPositiveNumberConstraint(boolean includeGroup) {
        GenericConstraintDef<PositiveNumber> constraint = new GenericConstraintDef<>(PositiveNumber.class);

        if (includeGroup) {
            constraint.groups(PositiveNumber.Group.class);
        }

        return constraint;
    }

    private ConstraintDef createAlbumTypeConstraint(boolean includeGroup, AlbumType... allowedTypes) {
        GenericConstraintDef<ValidAlbumType> constraint = new GenericConstraintDef<>(ValidAlbumType.class);

        if (includeGroup) {
            constraint.groups(ValidAlbumType.Group.class);
        }

        constraint.param("allowedTypes", allowedTypes);

        return constraint;
    }

    private ConstraintDef createAlbumTitleConstraint(boolean includeGroup) {
        GenericConstraintDef<ValidAlbumTitle> constraint = new GenericConstraintDef<>(ValidAlbumTitle.class);

        if (includeGroup) {
            constraint.groups(ValidAlbumTitle.Group.class);
        }

        return constraint;
    }
}
