package com.levi.javaxrestvalidation.validators.studio;

import com.levi.javaxrestvalidation.validators.musician.ValidAge;
import com.levi.javaxrestvalidation.validators.musician.ValidSurname;
import com.levi.javaxrestvalidation.validators.song.ValidName;

import javax.validation.GroupSequence;

@GroupSequence({
        ValidName.Group.class,
        ValidSurname.Group.class,
        ValidAge.Group.class
})
public interface StudioGroupSequence {
}
