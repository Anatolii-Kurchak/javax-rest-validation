package com.levi.javaxrestvalidation.validators.country;

import com.levi.javaxrestvalidation.util.SkipValidationRule;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidCountryIso3Validator implements ConstraintValidator<ValidCountryIso3, String> {

   private static final int THREE_CHARACTERS = 3;

   private SkipValidationRule skipValidationRule;

   public void initialize(ValidCountryIso3 annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(String iso3, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(iso3)) {
         return true;
      }

      return StringUtils.isNotBlank(iso3) && StringUtils.isAlpha(iso3) && StringUtils.length(iso3) == THREE_CHARACTERS;
   }
}
