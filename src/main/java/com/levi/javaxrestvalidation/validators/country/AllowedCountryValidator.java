package com.levi.javaxrestvalidation.validators.country;

import com.levi.javaxrestvalidation.entity.Country;
import com.levi.javaxrestvalidation.util.SkipValidationRule;
import com.levi.javaxrestvalidation.util.SupportedCountries;
import com.levi.javaxrestvalidation.validators.CountryContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AllowedCountryValidator implements ConstraintValidator<AllowedCountry, String> {

   private final CountryContext countryContext;

   private SkipValidationRule skipValidationRule;

   private List<String> supportedCountries;

   @Autowired
   public AllowedCountryValidator(CountryContext countryContext) {
      this.countryContext = countryContext;
   }

   public void initialize(AllowedCountry annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
      this.supportedCountries = Arrays.stream(annotation.countries())
              .map(SupportedCountries::getCountry)
              .collect(Collectors.toList());
   }

   public boolean isValid(String iso3, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(iso3)) {
         return true;
      }

      Country country = countryContext.findByIso3(iso3);

      return supportedCountries.contains(country.getName());
   }
}
