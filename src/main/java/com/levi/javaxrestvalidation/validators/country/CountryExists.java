package com.levi.javaxrestvalidation.validators.country;

import com.levi.javaxrestvalidation.util.SkipValidationRule;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER,ElementType.FIELD})
@Constraint(validatedBy = CountryExistsValidator.class)
public @interface CountryExists {

    String message() default "{com.levi.javax.rest.validation.existing.country}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    SkipValidationRule skipValidationRule() default SkipValidationRule.OBJECT_IS_NULL;

    interface Group{}
}