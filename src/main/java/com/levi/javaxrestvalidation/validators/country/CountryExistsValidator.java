package com.levi.javaxrestvalidation.validators.country;

import com.levi.javaxrestvalidation.util.SkipValidationRule;
import com.levi.javaxrestvalidation.validators.CountryContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CountryExistsValidator implements ConstraintValidator<CountryExists, String> {

   private final CountryContext countryContext;

   private SkipValidationRule skipValidationRule;

   @Autowired
   public CountryExistsValidator(CountryContext countryContext) {
      this.countryContext = countryContext;
   }

   public void initialize(CountryExists annotation) {
      this.skipValidationRule = annotation.skipValidationRule();
   }

   public boolean isValid(String iso3, ConstraintValidatorContext context) {
      if (skipValidationRule.shouldSkipValidation(iso3)) {
         return true;
      }

      return countryContext.findByIso3(iso3) != null;
   }
}
