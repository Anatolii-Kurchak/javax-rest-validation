package com.levi.javaxrestvalidation.validators.country;

import com.levi.javaxrestvalidation.util.SkipValidationRule;
import com.levi.javaxrestvalidation.util.SupportedCountries;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Constraint(validatedBy = AllowedCountryValidator.class)
public @interface AllowedCountry {

    String message() default "{com.levi.javax.rest.validation.allowed.country}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    SkipValidationRule skipValidationRule() default SkipValidationRule.OBJECT_IS_NULL;

    SupportedCountries[] countries() default {SupportedCountries.USA, SupportedCountries.GBR};

    interface Group{}
}
