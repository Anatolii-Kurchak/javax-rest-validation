package com.levi.javaxrestvalidation.model;

import com.levi.javaxrestvalidation.validators.song.ValidName;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class MusicStudio {
    @ValidName(groups = ValidName.Group.class)
    private String studioName;
    @Valid
    private List<Producer> producers;
}