package com.levi.javaxrestvalidation.model;

import com.levi.javaxrestvalidation.validators.musician.ValidAge;
import com.levi.javaxrestvalidation.validators.musician.ValidSurname;
import com.levi.javaxrestvalidation.validators.song.ValidName;
import lombok.Data;

@Data
public class Producer {
    @ValidName(groups = ValidName.Group.class)
    private String name;
    @ValidSurname(groups = ValidSurname.Group.class)
    private String surname;
    @ValidAge(groups = ValidAge.Group.class, min = 30, max = 55)
    private Integer age;
}
