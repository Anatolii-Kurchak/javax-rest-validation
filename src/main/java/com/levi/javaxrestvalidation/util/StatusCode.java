package com.levi.javaxrestvalidation.util;

import org.springframework.http.HttpStatus;

import java.util.Comparator;

public enum StatusCode {

    BAD_REQUEST(HttpStatus.BAD_REQUEST, 0),
    FORBIDDEN(HttpStatus.FORBIDDEN, 1),
    NOT_FOUND(HttpStatus.NOT_FOUND, 2),
    UNPROCESSABLE_ENTITY(HttpStatus.UNPROCESSABLE_ENTITY, 3);

    private HttpStatus httpStatus;
    private int priority;

    StatusCode(HttpStatus httpStatus, int priority) {
        this.httpStatus = httpStatus;
        this.priority = priority;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public static final Comparator<StatusCode> PRIORITY_COMPARATOR = Comparator.comparingInt(s -> s.priority);
}
