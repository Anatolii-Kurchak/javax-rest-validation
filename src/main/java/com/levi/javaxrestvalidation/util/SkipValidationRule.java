package com.levi.javaxrestvalidation.util;

import java.util.Objects;
import java.util.function.Predicate;

public enum SkipValidationRule {
    NONE(o -> false),
    OBJECT_IS_NULL(Objects::isNull);

    private Predicate<Object> skipValidationPredicate;

    SkipValidationRule(Predicate<Object> skipValidationPredicate) {
        this.skipValidationPredicate = skipValidationPredicate;
    }

    public boolean shouldSkipValidation(Object parameter) {
        return skipValidationPredicate.test(parameter);
    }
}
