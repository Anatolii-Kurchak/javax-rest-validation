package com.levi.javaxrestvalidation.util;

public enum AlbumType {
    DEMO_ALBUM,
    REMIX_ALBUM,
    STUDIO_ALBUM,
    SOLO,
    SOUNDTRACK_ALBUM;
}
