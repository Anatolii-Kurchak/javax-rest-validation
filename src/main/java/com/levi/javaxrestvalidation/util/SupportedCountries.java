package com.levi.javaxrestvalidation.util;

public enum SupportedCountries {
    UA("Ukraine"),
    GBR("Great Britain"),
    USA("United States");

    private String country;

    SupportedCountries(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}
