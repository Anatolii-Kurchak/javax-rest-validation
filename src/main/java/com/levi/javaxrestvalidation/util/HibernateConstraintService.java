package com.levi.javaxrestvalidation.util;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.util.Collection;
import java.util.Map;

@Component
public class HibernateConstraintService {

    public void addConstraintViolations(ConstraintValidatorContext context, String messageTemplate, Collection<String> invalidValues) {
        HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);
        hibernateContext.disableDefaultConstraintViolation();

        for (String value : invalidValues) {
            hibernateContext.addExpressionVariable("validatedValue", value)
                    .buildConstraintViolationWithTemplate(messageTemplate)
                    .addConstraintViolation();
        }
    }

    public void addConstraintViolations(ConstraintValidatorContext context, String messageTemplate, Collection<String> invalidValues, Map<String, Object> messageParameters) {
        HibernateConstraintValidatorContext hibernateContext = context.unwrap(HibernateConstraintValidatorContext.class);
        hibernateContext.disableDefaultConstraintViolation();

        for (String value : invalidValues) {
            messageParameters.forEach(hibernateContext::addMessageParameter);

            hibernateContext.addExpressionVariable("validatedValue", value)
                    .buildConstraintViolationWithTemplate(messageTemplate)
                    .addConstraintViolation();
        }
    }
}
