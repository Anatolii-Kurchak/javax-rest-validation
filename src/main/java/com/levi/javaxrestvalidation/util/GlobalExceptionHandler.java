package com.levi.javaxrestvalidation.util;

import com.levi.javaxrestvalidation.model.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final String STATUS_CODE = "statusCode";

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<Collection<Error>> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        Set<ConstraintViolation<?>> violations = extractConstraintViolations(e);

        List<Error> errors = createErrors(violations);

        HttpStatus httpStatus = resolveHttpStatus(violations);

        return ResponseEntity.status(httpStatus).body(errors);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<Collection<Error>> handleConstraintViolationException(ConstraintViolationException e) {
        List<Error> errors = createErrors(e.getConstraintViolations());

        HttpStatus httpStatus = resolveHttpStatus(e.getConstraintViolations());

        return ResponseEntity.status(httpStatus).body(errors);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    public ResponseEntity<Error> handleMissingParameterException(MissingServletRequestParameterException e) {
        Error error = new Error().message(e.getMessage()).date(OffsetDateTime.now());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    private Set<ConstraintViolation<?>> extractConstraintViolations(MethodArgumentNotValidException e) {
        return e.getAllErrors().stream()
                .map(error -> error.unwrap(ConstraintViolation.class))
                .map(v -> (ConstraintViolation<?>)v)
                .collect(Collectors.toSet());
    }

    private List<Error> createErrors(Set<ConstraintViolation<?>> violations) {
        return violations.stream()
                .map(violation -> new Error().message(violation.getMessage()).date(OffsetDateTime.now()))
                .collect(Collectors.toList());
    }

    private HttpStatus resolveHttpStatus(Collection<ConstraintViolation<?>> constraintViolations) {
        return constraintViolations.stream()
                .map(v -> v.getConstraintDescriptor().getAttributes().get(STATUS_CODE))
                .filter(Objects::nonNull)
                .map(StatusCode.class::cast)
                .distinct()
                .max(StatusCode.PRIORITY_COMPARATOR)
                .orElse(StatusCode.BAD_REQUEST)
                .getHttpStatus();
    }
}
