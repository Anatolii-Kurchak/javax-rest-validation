package com.levi.javaxrestvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaxRestValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaxRestValidationApplication.class, args);
	}

}
