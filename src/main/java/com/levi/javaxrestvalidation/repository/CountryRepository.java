package com.levi.javaxrestvalidation.repository;

import com.levi.javaxrestvalidation.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CountryRepository extends JpaRepository<Country, String>, QuerydslPredicateExecutor<Country> {
    Country findByIso3(String iso3);
}
