package com.levi.javaxrestvalidation.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;

import javax.validation.Validation;
import javax.validation.Validator;

@Configuration
public class Config {

    @Bean
    public Validator validator(AutowireCapableBeanFactory autowireCapableBeanFactory) {
        return Validation.byProvider(HibernateValidator.class)
                .configure()
                .constraintValidatorFactory(new SpringConstraintValidatorFactory(autowireCapableBeanFactory))//https://stackoverflow.com/questions/37958145/autowired-gives-null-value-in-custom-constraint-validator
                .allowOverridingMethodAlterParameterConstraint(true)//https://docs.jboss.org/hibernate/validator/5.3/reference/en-US/html/ch11.html#_public_api
//                .allowMultipleCascadedValidationOnReturnValues(true)
//                .allowParallelMethodsDefineParameterConstraints(true)
                .buildValidatorFactory()
                .getValidator();
    }
}